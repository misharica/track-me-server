/*
 * Display.h
 *
 *  Created on: Sep 26, 2020
 *      Author: rtrk
 */

#ifndef DISPLAY_H_
#define DISPLAY_H_

#define I2C_ADDR    0x27
#define LOG_TAG     "Display"

#include <stdint.h>

enum DispErr {
    DISP_ERROR_OK,
    DISP_ERROR_INIT_FAILED,
    DISP_ERROR_DISP_ALREADY_INITIALIZED,
    DISP_ERROR_DISP_NOT_INITIALIZED,
    DISP_ERROR_BAD_ARGUMENT
};

enum DispBitMode {
    DISP_4_BIT_MODE,
    DISP_8_BIT_MODE
};

enum DispLineMode {
    DISP_1_LINE_MODE,
    DISP_2_LINE_MODE
};

enum DispCharFont {
    DISP_FONT_5x8,
    DISP_FONT_5x10
};

enum DispAdressIncrement {
    DISP_ADDR_INC,
    DISP_ADDR_DEC
};

class Display {
public:
    Display();
    virtual ~Display();
    void turnBckLightOn();
    void turnBckLightOff();
    DispErr initialize(DispBitMode bitMode, DispLineMode lineMode, DispCharFont font);
    DispErr setDispOn();
    DispErr setDispOff();
    DispErr setCursorOn();
    DispErr setCursorOff();
    DispErr setBlinkOn();
    DispErr setBlinkOff();
    DispErr writeChar(char c);
    DispErr setEntryModeDispShift();
    DispErr setCursorPosition(uint8_t line, uint8_t pos);
    DispErr writeText(const char* text, uint8_t size);

private:
    void toggleEnable();
    void sendCommand(uint8_t cmd);
    DispErr setBitMode(DispBitMode mode);

    int mFd;
    uint8_t mByte;
    uint8_t mCurrentAddr;
    bool mBckLight;
    bool mInitialized;
    bool mDispOn;
    bool mCursorOn;
    bool mBlinkOn;
    bool mDispShift;
    DispBitMode mBitMode;
    DispLineMode mLineMode;
    DispCharFont mFont;
    DispAdressIncrement mAddrInc;

};

#endif /* DISPLAY_H_ */
