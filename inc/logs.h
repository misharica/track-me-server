/*
 * logs.h
 *
 *  Created on: Sep 26, 2020
 *      Author: rtrk
 */

#ifndef LOGS_H_
#define LOGS_H_

#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <iostream>
using namespace std;

#define DEBUG_LEVEL_ALL     0
#define DEBUG_LEVEL_VERBOSE 1
#define DEBUG_LEVEL_INFO    2
#define DEBUG_LEVEL_WARNING 3
#define DEBUG_LEVEL_ERROR   4
#define DEBUG_LEVEL_SILENT  5

static unsigned char DEBUG_LEVEL = DEBUG_LEVEL_INFO;

#define _MAX_TEXT_COLOR_LEN_    (16)
#define _PRINT_BUFFER_SIZE_     (102400-_MAX_TEXT_COLOR_LEN_)

#define RESET        0
#define BRIGHT       1
#define DIM          2
#define UNDERLINE    3
#define BLINK        4
#define REVERSE      7
#define HIDDEN       8

#define BLACK        0
#define RED          1
#define GREEN        2
#define YELLOW       3
#define BLUE         4
#define MAGENTA      5
#define CYAN         6
#define WHITE        7
#define NONE         8
#define BACKGROUND   NONE

#define __FILENAME__(FULL_PATH) (strrchr(FULL_PATH, '/') ? strrchr(FULL_PATH, '/') + 1 : FULL_PATH)

#define DBG_VERBOSE(fmt, args...) _DBG_VERBOSE(__FILE__, __FUNCTION__, __LINE__, fmt, ## args)
#define DBG_INFO(fmt, args...) _DBG_INFO(__FILE__, __FUNCTION__, __LINE__, fmt, ## args)
#define DBG_WARNING(fmt, args...) _DBG_WARNING( __FILE__, __FUNCTION__, __LINE__, fmt, ## args)
#define DBG_ERROR(fmt, args...) _DBG_ERROR(__FILE__, __FUNCTION__, __LINE__, fmt, ## args)

static inline int textcolor(char* buffer, int buffer_len, int attr, int fg, int bg) {
    return snprintf(buffer, buffer_len, "%c[%d;%d;%dm", 0x1B, attr, fg + 30, bg + 40);
}

static inline int printFuncLine(char* buffer, int buffer_len, __attribute__ ((unused))const char* file,
    const char* function, int line)
{
    char* cur_buf_pos = buffer;
    char* buf_end = buffer + buffer_len;

    cur_buf_pos += textcolor(cur_buf_pos, buf_end - cur_buf_pos, BRIGHT, CYAN, BACKGROUND);
    cur_buf_pos += snprintf(cur_buf_pos, buf_end - cur_buf_pos, "[%s, %s:%d] ", __FILENAME__(file), function, line);
    cur_buf_pos += textcolor(cur_buf_pos, buf_end - cur_buf_pos, RESET, NONE, BACKGROUND);

    return cur_buf_pos - buffer;
}

static inline void _DBG_VERBOSE(const char* file, const char* function, int line, const char * format, ...)
{
    if(DEBUG_LEVEL_VERBOSE < DEBUG_LEVEL) {
        return;
    }

    char buffer[_PRINT_BUFFER_SIZE_ + _MAX_TEXT_COLOR_LEN_];
    char* buf_end = buffer + _PRINT_BUFFER_SIZE_; // save space for closing color
    char* cur_buf_pos = buffer;

    cur_buf_pos += textcolor(cur_buf_pos, buf_end - cur_buf_pos, BRIGHT, NONE, BACKGROUND);
    cur_buf_pos += snprintf(cur_buf_pos, buf_end - cur_buf_pos, "VERBOSE: ");

    cur_buf_pos += printFuncLine(cur_buf_pos, buf_end - cur_buf_pos, file, function, line);

    va_list argptr;
    va_start(argptr, format);
    cur_buf_pos += vsnprintf(cur_buf_pos, buf_end - cur_buf_pos, format, argptr);
    va_end(argptr);

    if(cur_buf_pos >= buf_end)
    {
        textcolor(buf_end - 1, _MAX_TEXT_COLOR_LEN_, RESET, NONE, BACKGROUND);
    }
    else
    {
        textcolor(cur_buf_pos, _MAX_TEXT_COLOR_LEN_, RESET, NONE, BACKGROUND);
    }

    cout << buffer << endl;
}

static inline void _DBG_INFO(const char* file, const char* function, int line, const char * format, ...)
{
    if(DEBUG_LEVEL_INFO < DEBUG_LEVEL) {
        return;
    }
    char buffer[_PRINT_BUFFER_SIZE_ + _MAX_TEXT_COLOR_LEN_];
    char* buf_end = buffer + _PRINT_BUFFER_SIZE_; // save space for closing color
    char* cur_buf_pos = buffer;

    cur_buf_pos += textcolor(cur_buf_pos, buf_end - cur_buf_pos, BRIGHT, GREEN, BACKGROUND);
    cur_buf_pos += snprintf(cur_buf_pos, buf_end - cur_buf_pos, "INFO: ");

    cur_buf_pos += printFuncLine(cur_buf_pos, buf_end - cur_buf_pos, file, function, line);

    va_list argptr;
    va_start(argptr, format);
    cur_buf_pos += vsnprintf(cur_buf_pos, buf_end - cur_buf_pos, format, argptr);
    va_end(argptr);

    if(cur_buf_pos >= buf_end)
    {
        textcolor(buf_end - 1, _MAX_TEXT_COLOR_LEN_, RESET, NONE, BACKGROUND);
    }
    else
    {
        textcolor(cur_buf_pos, _MAX_TEXT_COLOR_LEN_, RESET, NONE, BACKGROUND);
    }

    cout << buffer << endl;
}

static inline void _DBG_WARNING(const char* file, const char* function, int line, const char * format, ...)
{
    if(DEBUG_LEVEL_WARNING < DEBUG_LEVEL) {
        return;
    }
    char buffer[_PRINT_BUFFER_SIZE_ + _MAX_TEXT_COLOR_LEN_];
    char* buf_end = buffer + _PRINT_BUFFER_SIZE_; // save space for closing color
    char* cur_buf_pos = buffer;

    cur_buf_pos += textcolor(cur_buf_pos, buf_end - cur_buf_pos, BRIGHT, YELLOW, BACKGROUND);
    cur_buf_pos += snprintf(cur_buf_pos, buf_end - cur_buf_pos, "WARNING: ");

    cur_buf_pos += printFuncLine(cur_buf_pos, buf_end - cur_buf_pos, file, function, line);

    va_list argptr;
    va_start(argptr, format);
    cur_buf_pos += vsnprintf(cur_buf_pos, buf_end - cur_buf_pos, format, argptr);
    va_end(argptr);

    if(cur_buf_pos >= buf_end)
    {
        textcolor(buf_end - 1, _MAX_TEXT_COLOR_LEN_, RESET, NONE, BACKGROUND);
    }
    else
    {
        textcolor(cur_buf_pos, _MAX_TEXT_COLOR_LEN_, RESET, NONE, BACKGROUND);
    }

    cout << buffer << endl;
}

static inline void _DBG_ERROR(const char* file, const char* function, int line, const char * format, ...)
{
    if(DEBUG_LEVEL_ERROR < DEBUG_LEVEL) {
        return;
    }
    char buffer[_PRINT_BUFFER_SIZE_ + _MAX_TEXT_COLOR_LEN_];
    char* buf_end = buffer + _PRINT_BUFFER_SIZE_; // save space for closing color
    char* cur_buf_pos = buffer;

    cur_buf_pos += textcolor(cur_buf_pos, buf_end - cur_buf_pos, BRIGHT, RED, BACKGROUND);
    cur_buf_pos += snprintf(cur_buf_pos, buf_end - cur_buf_pos, "ERROR: ");

    cur_buf_pos += printFuncLine(cur_buf_pos, buf_end - cur_buf_pos, file, function, line);

    va_list argptr;
    va_start(argptr, format);
    cur_buf_pos += vsnprintf(cur_buf_pos, buf_end - cur_buf_pos, format, argptr);
    va_end(argptr);

    if(cur_buf_pos >= buf_end)
    {
        textcolor(buf_end - 1, _MAX_TEXT_COLOR_LEN_, RESET, NONE, BACKGROUND);
    }
    else
    {
        textcolor(cur_buf_pos, _MAX_TEXT_COLOR_LEN_, RESET, NONE, BACKGROUND);
    }

    cout << buffer << endl;
}



#endif /* LOGS_H_ */
