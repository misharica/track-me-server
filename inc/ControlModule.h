/*
 * ControlModule.h
 *
 *  Created on: Oct 13, 2020
 *      Author: rtrk
 */

#ifndef CONTROLMODULE_H_
#define CONTROLMODULE_H_

#include "RemoteControl.h"

#include <thread>
#include <mutex>
#include <condition_variable>

class DisplayControl;
class RemoteControl;
class Server;
class UserLocation;
class DirectionApiProxy;

using namespace std;

class ControlModule {
public:
    ControlModule();
    virtual ~ControlModule();

    void attachDisplayControl(DisplayControl *dispCtrl) { mDisplayCtrl = dispCtrl; }
    void attachRemoteControl(RemoteControl *rc);
    void deattachRemoteControl();
    void onRcEventReceived(RcKeycode code);
    void attachServer(Server *srv);
    void deattachServer();
    void onSrvEventReceived(UserLocation location);
    void start();

private:
    DisplayControl *mDisplayCtrl;
    RemoteControl  *mRC;
    Server         *mSrv;

    bool                mLocationUpdated;
    bool                mDirectionsInfoThreadRunning;
    thread              *mRefreshDirectionsInfoThread;
    mutex               mDirectionsInfoMutex;
    condition_variable  mDirectionsInfoCv;
    void                refreshDirectionsTask();

    UserLocation        *mUserLocation;
    DirectionApiProxy   *mDirectionApiProxy;

};

#endif /* CONTROLMODULE_H_ */
