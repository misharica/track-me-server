/*
 * DisplayControl.h
 *
 *  Created on: Nov 27, 2020
 *      Author: rtrk
 */

#ifndef DISPLAYCONTROL_H_
#define DISPLAYCONTROL_H_

#include <UserLocation.h>
#include "RemoteControl.h"
#include <thread>
#include <mutex>
#include <condition_variable>

using namespace std;

enum DispCtrlErr {
  DISP_CTRL_ERR_OK,
  DISP_CTRL_ERR_NO_DISPLAY_ATTACHED
};

class Display;

class DisplayControl {
public:
    DisplayControl();
    virtual ~DisplayControl();

    void attachDisplay(Display *disp) { mDisplay = disp; }
    void onLocationUpdate(UserLocation location);
    void onRcEventReceived(RcKeycode code);
    DispCtrlErr start();
    void initDisplay();
    void stop();

private:
    void initHomeScreen();
    void updateSpeed(double speedMps);

private:
    Display     *mDisplay;
    UserLocation    mLocation;

    bool                mLocationUpdated;
    bool                mDispUpdateThreadRunning;
    thread              *mDispUpdateThread;
    mutex               mDispUpdateMutex;
    condition_variable  mDispUpdateCv;
    void displayUpdateTask();


};

#endif /* DISPLAYCONTROL_H_ */
