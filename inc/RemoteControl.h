/*
 * RemoteControle.h
 *
 *  Created on: Oct 11, 2020
 *      Author: rtrk
 */

#ifndef REMOTECONTROL_H_
#define REMOTECONTROL_H_

#include <time.h>

#define HX1838_OUTPUT_PIN 25

enum NEC_State {
    NEC_Idle,
    NEC_DetectingStart,
    NEC_Reading,
    NEC_RepeatDetected
};

enum RcKeycode {
    KEYCODE_POWER,
    KEYCODE_1
};

class ControlModule;

class RemoteControl {
public:
    RemoteControl();
    virtual ~RemoteControl();
    void printRc();
    void subscribeCMForEvents(ControlModule *cm) { mCM = cm; };
    void unsubscribeCMFromEvents() { mCM = NULL; };

private:
    NEC_State        mState;
    struct timespec  mLast;
    unsigned int     mBitCnt;
    ControlModule    *mCM;
};

#endif /* REMOTECONTROL_H_ */
