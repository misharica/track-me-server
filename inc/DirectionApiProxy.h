/*
 * DirectionApiProxy.h
 *
 *  Created on: Nov 30, 2020
 *      Author: rtrk
 */

#ifndef DIRECTIONAPIPROXY_H_
#define DIRECTIONAPIPROXY_H_

#include <stddef.h>
#include <string>

using namespace std;

typedef struct Distance {
    string text;
    unsigned int value; // in meters
} Distance_t;

typedef struct Duration {
    string text;
    unsigned int value; // in second
} Duration_t;

typedef struct Location {
    double lat;
    double lng;
} Location_t;

typedef struct Directions {
    Distance_t distance;
    Duration_t duration;
    Location_t startLoc;
    Location_t endLoc;
    string     startAddr;
    string     endAddr;
} Directions_t;

typedef struct Response {
    char* data;
    size_t size;
} Response_t;

enum DirectionsApiError {
    DIR_API_ERR_OK,
    DIR_API_ERR_JSON_PARSING_FAILED
};

class UserLocation;

class DirectionApiProxy {
public:
    DirectionApiProxy();
    virtual ~DirectionApiProxy();
    void updateDirections(UserLocation *location);

private:
    class UrlBuilder {
    public:
        UrlBuilder(UserLocation *location);
        virtual ~UrlBuilder();
        char* getUrl() { return mUrl; };
    private:
        char* mUrl;
    };

private:
    size_t writeDataCallback(void *contents, size_t size, size_t nmemb, void *userp);
    DirectionsApiError parseJson(const char* json);

private:
    Directions_t mDirections;
    Response_t   mResponse;
};

#endif /* DIRECTIONAPIPROXY_H_ */
