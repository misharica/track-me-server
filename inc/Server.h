/*
 * Server.h
 *
 *  Created on: Oct 24, 2020
 *      Author: rtrk
 */

#ifndef SERVER_H_
#define SERVER_H_

#include <string>
#include <thread>

#include <openssl/ssl.h>

#define CERTF "/home/pi/CA/misharica.duckdns.org.crt"
#define KEYF  "/home/pi/CA/misharica.duckdns.org.key"

enum SrvErr {
  SRV_ERR_OK,
  SRV_ERR_GENERAL,
  SRV_ERR_BAD_PARAM
};

enum SrvResponseCode {
  CODE_200_OK,
  CODE_400_BAD_REQUEST
};

using namespace std;

class ControlModule;
class UserLocation;

class Server {

public:
    Server();
    virtual ~Server();
    SrvErr start(const char* port);
    void subscribeCMForEvents(ControlModule *cm) { mCM = cm; };

private:
    SrvErr initSSL();
    SrvErr initTcpSocket(const char* port);
    SrvErr threadStart();
    SrvErr threadStop();
    void   readingTask();
    string getOpenSSLError();
    SrvResponseCode processRequest(const char* buffer, const size_t size);
    double extractParam(const char* buffer, const char* param);
    SrvErr generateResponse(char **buffer, long *numbytes, SrvResponseCode code);
    void   notifyCM();

    int            mSocket_fd;
    bool           mThreadRunning;
    thread         *mThread;
    SSL_CTX        *mCtx;
    UserLocation   *mLocation;
    ControlModule  *mCM;

};

#endif /* SERVER_H_ */
