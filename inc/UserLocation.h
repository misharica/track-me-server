/*
 * Location.h
 *
 *  Created on: Nov 26, 2020
 *      Author: rtrk
 */

#ifndef USERLOCATION_H_
#define USERLOCATION_H_

class UserLocation {
private:
    double mLatitude;
    double mLongitude;
    double mSpeed;

public:
    UserLocation();
    virtual ~UserLocation();

    double getLatitude() const {
        return mLatitude;
    }

    void setLatitude(double latitude) {
        this->mLatitude = latitude;
    }

    double getLongitude() const {
        return mLongitude;
    }

    void setLongitude(double longitude) {
        this->mLongitude = longitude;
    }

    double getSpeed() const {
        return mSpeed;
    }

    void setSpeed(double speed) {
        this->mSpeed = speed;
    }
};

#endif /* USERLOCATION_H_ */
