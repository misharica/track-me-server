/*
 * DisplayControl.cpp
 *
 *  Created on: Nov 27, 2020
 *      Author: rtrk
 */

#include <DisplayControl.h>
#include <Display.h>
#include <logs.h>
#include <unistd.h>
#include <UserLocation.h>

#include <sstream>
#include <iomanip>

#define SPEED_FORMAT_1      "  %.1f"
#define SPEED_FORMAT_10     " %.1f"
#define SPEED_FORMAT_100    "%.1f"

using namespace std;

DisplayControl::DisplayControl() {
    // TODO Auto-generated constructor stub
    mDisplay                 = NULL;
    mDispUpdateThread        = NULL;
    mDispUpdateThreadRunning = false;
    mLocationUpdated         = false;
}

DisplayControl::~DisplayControl() {
    // TODO Auto-generated destructor stub
}



void DisplayControl::onLocationUpdate(UserLocation location) {

    DBG_INFO("onLocationUpdate: mLatitude[%f] mLongitude[%f] mSpeed[%f]",
            location.getLatitude(), location.getLongitude(), location.getSpeed());

    unique_lock<mutex> locker(mDispUpdateMutex);

    mLocation = location;
    mLocationUpdated = true;

    locker.unlock();
    mDispUpdateCv.notify_one();

}

void DisplayControl::onRcEventReceived(RcKeycode code) {
    DBG_INFO("code[%d]", code);
}

DispCtrlErr DisplayControl::start() {

    if(mDisplay == NULL) {
        DBG_ERROR("No display attached...");
        return DISP_CTRL_ERR_NO_DISPLAY_ATTACHED;
    }

    initDisplay();

    mDispUpdateThreadRunning = true;
    mDispUpdateThread = new thread(&DisplayControl::displayUpdateTask, this);

    return DISP_CTRL_ERR_OK;

}

void DisplayControl::initDisplay() {
    mDisplay->initialize(DISP_4_BIT_MODE, DISP_2_LINE_MODE, DISP_FONT_5x8);
    initHomeScreen();
}

void DisplayControl::initHomeScreen() {
    mDisplay->setDispOn();

    mDisplay->setCursorPosition(1, 1);
    mDisplay->writeText("Arriv:     18:00", strlen("Arriv:     18:00"));

    mDisplay->setCursorPosition(2, 1);
    mDisplay->writeText("Speed:", strlen("Speed:"));
    mDisplay->setCursorPosition(2, 14);
    mDisplay->writeText("0.0", strlen("0.0"));
}

/*
 * @param speed - speed in m/s
 * */
void DisplayControl::updateSpeed(double speedMps) {
    double speedKmph;
    char* speedKmph_string;

    if(speedMps < 0 || speedMps > 1000) {
        // better return some error
        return;
    }

    speedKmph = speedMps * 3.6;
    if (speedKmph < 10 ) {
        speedKmph_string = (char*)calloc(strlen(SPEED_FORMAT_1), sizeof(char));
        sprintf(speedKmph_string, SPEED_FORMAT_1, speedKmph);
    } else if (speedKmph >= 10 && speedKmph < 100) {
        speedKmph_string = (char*)calloc(strlen(SPEED_FORMAT_10), sizeof(char));
        sprintf(speedKmph_string, SPEED_FORMAT_10, speedKmph);
    } else {
        speedKmph_string = (char*)calloc(strlen(SPEED_FORMAT_100), sizeof(char));
        sprintf(speedKmph_string, SPEED_FORMAT_100, speedKmph);
    }

    mDisplay->setCursorPosition(2, 12);
    mDisplay->writeText(speedKmph_string, strlen(speedKmph_string));

    free(speedKmph_string);

}

void DisplayControl::stop() {

    mDispUpdateThreadRunning = false;
    mDispUpdateThread->join();
    delete mDispUpdateThread;

}

void DisplayControl::displayUpdateTask() {

    while(mDispUpdateThreadRunning) {

        unique_lock<mutex> locker(mDispUpdateMutex);
        mDispUpdateCv.wait(locker, [this]{return mLocationUpdated;});

        mLocationUpdated = false;

        updateSpeed(mLocation.getSpeed());

        locker.unlock();

    }

}
