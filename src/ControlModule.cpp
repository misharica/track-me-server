/*
 * ControlModule.cpp
 *
 *  Created on: Oct 13, 2020
 *      Author: rtrk
 */

#include <ControlModule.h>
#include <RemoteControl.h>
#include <Server.h>
#include <DisplayControl.h>
#include <DirectionApiProxy.h>
#include <iostream>
#include <logs.h>
#include <UserLocation.h>

ControlModule::ControlModule() {
    // TODO Auto-generated constructor stub
    mDisplayCtrl    = NULL;
    mRC             = NULL;
    mSrv            = NULL;

    mLocationUpdated             = false;
    mDirectionsInfoThreadRunning = false;
    mRefreshDirectionsInfoThread = NULL;

    mUserLocation = new UserLocation();
    mDirectionApiProxy = new DirectionApiProxy();
}

ControlModule::~ControlModule() {
    // TODO Auto-generated destructor stub
    mDisplayCtrl    = NULL;
    mRC             = NULL;

    delete mDirectionApiProxy;
    mDirectionApiProxy = NULL;

    delete mUserLocation;
    mUserLocation = NULL;
}

void ControlModule::attachRemoteControl(RemoteControl *rc) {
    mRC = rc;
    mRC->subscribeCMForEvents(this);
}

void ControlModule::deattachRemoteControl() {
    mRC->unsubscribeCMFromEvents();
    mRC = NULL;
}

void ControlModule::onRcEventReceived(RcKeycode code) {

    if(mDisplayCtrl == NULL) {
        DBG_WARNING("No display control attached!");
    }

    mDisplayCtrl->onRcEventReceived(code);

}

void ControlModule::attachServer(Server *srv) {
    mSrv = srv;
    mSrv->subscribeCMForEvents(this);
}

void ControlModule::deattachServer() {
    mSrv = NULL;
}

/*
 * update with new location, signal and quickly return from this callback
 *
 * */
void ControlModule::onSrvEventReceived(UserLocation location) {

    if(mDisplayCtrl == NULL) {
        DBG_WARNING("No display control attached!");
    }

    unique_lock<mutex> locker(mDirectionsInfoMutex);

    // update user location
    DBG_INFO("onSrvEventReceived");
    *mUserLocation = location;
    mLocationUpdated = true;

    locker.unlock();
    mDirectionsInfoCv.notify_one();

//    mDisplayCtrl->onLocationUpdate(location);

}

void ControlModule::start() {

    mDirectionsInfoThreadRunning = true;
    mRefreshDirectionsInfoThread = new thread(&ControlModule::refreshDirectionsTask, this);

}

void ControlModule::refreshDirectionsTask() {

    while (mDirectionsInfoThreadRunning) {

        unique_lock<mutex> locker(mDirectionsInfoMutex);
        mDirectionsInfoCv.wait(locker, [this]{return mLocationUpdated;});

        mLocationUpdated = false;

        //update directions
        DBG_INFO("Updating directions from Google API");
        mDirectionApiProxy->updateDirections(mUserLocation);
//        mDisplayCtrl->onLocationUpdate(mDirectionApiProxy);

        locker.unlock();
    }

}


