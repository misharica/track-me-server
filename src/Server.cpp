/*
 * Server.cpp
 *
 *  Created on: Oct 24, 2020
 *      Author: rtrk
 */

#include <logs.h>
#include <Server.h>
#include <ControlModule.h>

#include <string>

#include <openssl/ssl.h>
#include <openssl/err.h>

#include <netdb.h>
#include <unistd.h>
#include <UserLocation.h>

using namespace std;

#define BACKLOG     10

#define PARAM_SEPARATOR "&"
#define PARAM_EQUAL     "="
#define PARAM_LATITUDE  "mLatitude"
#define PARAM_LONGITUDE "mLongitude"
#define PARAM_SPEED     "mSpeed"

#define FILE_200_OK "/home/pi/CrossComp/response_200_ok.html"
#define FILE_400_BAD_REQUEST "/home/pi/CrossComp/response_400_bad_request.html"

SrvErr Server::generateResponse(char **buffer, long *numbytes, SrvResponseCode code) {

    /* declare a file pointer */
     FILE    *infile;

     /* open an existing file for reading */
     switch(code) {
         case CODE_200_OK:
             infile = fopen(FILE_200_OK, "r");
             break;
         case CODE_400_BAD_REQUEST:
             infile = fopen(FILE_400_BAD_REQUEST, "r");
             break;
         default:
             break;
     }

     /* quit if the file does not exist */
     if(infile == NULL) {
         DBG_ERROR("index.html doesn't exist");
         return SRV_ERR_GENERAL;
     }

     /* Get the number of bytes */
     fseek(infile, 0L, SEEK_END);
     *numbytes = ftell(infile);

     /* reset the file position indicator to
     the beginning of the file */
     fseek(infile, 0L, SEEK_SET);

     /* grab sufficient memory for the
     buffer to hold the text */
     *buffer = (char*)calloc(*numbytes, sizeof(char));
     if(*buffer == NULL) {
         DBG_ERROR("buffer allocation failed");
         fclose(infile);
         return SRV_ERR_GENERAL;
     }

     /* copy all the text into the buffer */
     fread(*buffer, sizeof(char), *numbytes, infile);
     fclose(infile);

//     DBG_INFO("%s", *buffer);

     return SRV_ERR_OK;

}

Server::Server() {
    // TODO Auto-generated constructor stub
    mSocket_fd      = -1;
    mCtx            = NULL;
    mThread         = NULL;
    mCM             = NULL;
    mThreadRunning  = false;

    mLocation       = new UserLocation();
}

Server::~Server() {
    // TODO Auto-generated destructor stub
    SSL_CTX_free (mCtx);
    mCtx = NULL;
    delete mLocation;
    mLocation = NULL;
}

void Server::readingTask() {

    int sd;
    int err;
    size_t client_len;
    struct sockaddr_in sa_cli;
    SSL *ssl;
    char requestBuffer[4096];
    char *responseBuffer = NULL;
    long responseBufferSize;
    SrvResponseCode srvResponseCode;
    SrvErr srvErr;

    DBG_INFO("Reading thread started");

    client_len = sizeof(sa_cli);

    while(mThreadRunning) {
        sd = accept (mSocket_fd, (struct sockaddr*) &sa_cli, &client_len);
        if(sd == -1) {
            DBG_ERROR("Failed to accept new connection");
            close(sd);
            continue;
        }
        DBG_INFO("Connection accepted from %lx, port %x", sa_cli.sin_addr.s_addr, sa_cli.sin_port);

        ssl = SSL_new (mCtx);
        if(ssl == NULL) {
            DBG_ERROR("SSL_new failed");
            close(sd);
            continue;
        }
        SSL_set_fd (ssl, sd);
        err = SSL_accept (ssl);
        if(err == -1) {
            DBG_ERROR("SSL_accept failed: %s", getOpenSSLError().c_str());
            close(sd);
            SSL_free (ssl);
            continue;
        }

        err = SSL_read (ssl, requestBuffer, sizeof(requestBuffer) - 1);
        if(err == -1) {
            DBG_ERROR("SSL_read failed: %s", getOpenSSLError().c_str());
        }

        srvResponseCode = processRequest(requestBuffer, (size_t)err);
        if(srvResponseCode == CODE_200_OK) {
            notifyCM();
        }

        srvErr = generateResponse(&responseBuffer, &responseBufferSize, srvResponseCode);
        if(srvErr == SRV_ERR_OK) {
            err = SSL_write (ssl, responseBuffer, responseBufferSize);
            if(err == -1) {
                DBG_ERROR("SSL_write failed: %s", getOpenSSLError().c_str());
            }
        }

        /* free the memory we used for the buffer */
        free(responseBuffer);

        /* Clean up. */
        close (sd);
        SSL_free (ssl);
    }

    DBG_INFO("Reading thread stopped");
}

SrvErr Server::initSSL() {

    const SSL_METHOD *meth;

    SSL_load_error_strings();
    OpenSSL_add_ssl_algorithms();
    meth = TLS_server_method();
    mCtx = SSL_CTX_new (meth);
    if (!mCtx) {
        DBG_ERROR("SSL_CTX_new failed: %s", getOpenSSLError().c_str());
        return SRV_ERR_GENERAL;
    }

    if (SSL_CTX_use_certificate_file(mCtx, CERTF, SSL_FILETYPE_PEM) <= 0) {
        DBG_ERROR("SSL_CTX_use_certificate_file failed: %s", getOpenSSLError().c_str());
        return SRV_ERR_GENERAL;
    }
    if (SSL_CTX_use_PrivateKey_file(mCtx, KEYF, SSL_FILETYPE_PEM) <= 0) {
        DBG_ERROR("SSL_CTX_use_PrivateKey_file failed: %s", getOpenSSLError().c_str());
        return SRV_ERR_GENERAL;
    }

    if (!SSL_CTX_check_private_key(mCtx)) {
        DBG_ERROR("Private key does not match the certificate public key\n");
        return SRV_ERR_GENERAL;
    }

    return SRV_ERR_OK;
}

SrvErr Server::initTcpSocket(const char* port) {

    int err;
    struct addrinfo hints, *srvInfo, *p;

    memset(&hints, 0x00, sizeof(struct addrinfo));
    hints.ai_family   = AF_UNSPEC;   // use IPv4 or IPv6, whichever
    hints.ai_socktype = SOCK_STREAM; // TCP stream sockets
    hints.ai_flags    = AI_PASSIVE;  // fill in my IP for me

    err = getaddrinfo(NULL, port, &hints, &srvInfo);
    if(err) {
        DBG_ERROR("getaddrinfo failed: %s", gai_strerror(err));
        return SRV_ERR_GENERAL;
    }

    /* loop through all the results and bind to the first we can */
    for(p = srvInfo; p != NULL; p = srvInfo->ai_next) {
        mSocket_fd = socket(srvInfo->ai_family, srvInfo->ai_socktype, srvInfo->ai_protocol);
        if(mSocket_fd == -1) {
            DBG_ERROR("Creating socket file descriptor failed");
            continue;
        }

        /* make socket reusable */
        int yes = 1;
        if (setsockopt(mSocket_fd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof yes) == -1) {
            DBG_WARNING("setsockopt failed");
        }

        err = bind(mSocket_fd, srvInfo->ai_addr, srvInfo->ai_addrlen);
        if(err == -1) {
            DBG_ERROR("Socket binding failed");
            continue;
        }

        break;
    }

    freeaddrinfo(srvInfo);

    if(p == NULL) {
        DBG_ERROR("Failed to bind to any socket");
        return SRV_ERR_GENERAL;
    }

    err = listen(mSocket_fd, BACKLOG);
    if(err == -1) {
        DBG_ERROR("Failed to put the socket into listening state");
        return SRV_ERR_GENERAL;
    }

    return SRV_ERR_OK;
}

SrvErr Server::threadStart() {

    mThreadRunning = true;
    mThread = new thread(&Server::readingTask, this);

    return SRV_ERR_OK;
}

SrvErr Server::threadStop() {

    mThreadRunning = false;
    mThread->join();
    delete mThread;

    return SRV_ERR_OK;
}

SrvErr Server::start(const char* port) {

    if(initSSL() != SRV_ERR_OK) {
        return SRV_ERR_GENERAL;
    }
    if(initTcpSocket(port) != SRV_ERR_OK) {
        return SRV_ERR_GENERAL;
    }
    if(threadStart() != SRV_ERR_OK) {
        return SRV_ERR_GENERAL;
    }

    return SRV_ERR_OK;
}

string Server::getOpenSSLError()
{

    BIO *bio = BIO_new(BIO_s_mem());
    ERR_print_errors(bio);
    char *buf;
    size_t len = BIO_get_mem_data(bio, &buf);
    string ret(buf, len);
    BIO_free(bio);
    return ret;
}

SrvResponseCode Server::processRequest(const char* buffer, const size_t size) {

    double extractedParam = 0;

    extractedParam = extractParam(buffer, PARAM_LATITUDE);
    if(extractedParam == -1) {
        DBG_WARNING("Param \'%s\' not found", PARAM_LATITUDE);
        return CODE_400_BAD_REQUEST;
    }
    mLocation->setLatitude(extractedParam);

    extractedParam = extractParam(buffer, PARAM_LONGITUDE);
    if(extractedParam == -1) {
        DBG_WARNING("Param \'%s\' not found", PARAM_LONGITUDE);
        return CODE_400_BAD_REQUEST;
    }
    mLocation->setLongitude(extractedParam);

    extractedParam = extractParam(buffer, PARAM_SPEED);
    if(extractedParam == -1) {
        DBG_WARNING("Param \'%s\' not found", PARAM_SPEED);
        return CODE_400_BAD_REQUEST;
    }
    mLocation->setSpeed(extractedParam);

    DBG_INFO("mLatitude[%f] mLongitude[%f] mSpeed[%f]",
            mLocation->getLatitude(), mLocation->getLongitude(), mLocation->getSpeed());

    return CODE_200_OK;

}

double Server::extractParam(const char* buffer, const char* param) {

    const char *start  = NULL;
    const char *end    = NULL;
    char* resultString = NULL;
    double result = 0;
    size_t resSize = 0;

    start = strstr(buffer, param);
    if(start == NULL) {
        DBG_ERROR("Param \'%s\' not found", param);
        return -1;
    }
    start = start + strlen(param) + strlen(PARAM_EQUAL);

    end   = strstr(start , PARAM_SEPARATOR);
    if(end == NULL) {
        DBG_ERROR("Param \'%s\' not found", param);
        return -1;
    }

    resSize = end - start + 1;
    resultString = (char*)calloc(resSize, sizeof(char));
    if(resultString == NULL) {
        DBG_ERROR("Memory alocation failed!");
        return -1;
    }

    memcpy(resultString, start, resSize);
    resultString[resSize-1] = '\0';

    result = atof(resultString);

    free(resultString);

    return result;

}

void Server::notifyCM() {

    if(mCM == NULL) {
        DBG_WARNING("We are not attached to Command Module!");
        return;
    }

//    UserLocation location = *mLocation; // make a copy of the object
    mCM->onSrvEventReceived(*mLocation);

}




