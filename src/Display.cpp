/*
 * Display.cpp
 *
 *  Created on: Sep 26, 2020
 *      Author: rtrk
 */

#include <iostream>
#include "Display.h"
#include "wiringPiI2C.h"
#include <unistd.h>
#include "logs.h"

//remove!!!
#include <bitset>

#define DISP_CMD_CLEAR          0x01
#define DISP_CMD_RETURN_HOME    0x02

#define DISP_CMD_ENTRY_MODE     0x04 // performed during data read/write
#define DISP_OPT_DIR_INCREMENT  0x02
#define DISP_OPT_DIR_DECREMENT  0x00
#define DISP_OPT_DISP_SHIFT_ON  0x01
#define DISP_OPT_DISP_SHIFT_OFF 0x00

#define DISP_CMD_ON_OFF         0x08
#define DISP_OPT_DISP_ON        0x04
#define DISP_OPT_DISP_OFF       0x00
#define DISP_OPT_CURSOR_ON      0x02
#define DISP_OPT_CURSOR_OFF     0x00
#define DISP_OPT_BLINK_ON       0x01
#define DISP_OPT_BLINK_OFF      0x00

#define DISP_CMD_CUR_DISP_SHIFT 0x10 // performed without changing DDRAM content
#define DISP_OPT_SHIFT_DISP_ON  0x08
#define DISP_OPT_SHIFT_DISP_OFF 0x00
#define DISP_OPT_SHIFT_RIGHT    0x04
#define DISP_OPT_SHIFT_LEFT     0x00

#define DISP_CMD_FUNC_SET       0x20
#define DISP_OPT_DATA_LENGTH_8  0x10
#define DISP_OPT_DATA_LENGTH_4  0x00
#define DISP_OPT_LINES_NUM_2    0x08
#define DISP_OPT_LINES_NUM_1    0x00
#define DISP_OPT_DOTS_5x10      0x04 // only one line mode
#define DISP_OPT_DOTS_5x8       0x00

#define DISP_CMD_DDRAM_SET      0x80

#define DISP_PIN_D7             0x80
#define DISP_PIN_D6             0x40
#define DISP_PIN_D5             0x20
#define DISP_PIN_D4             0x10
#define DISP_PIN_LED            0x08 // low - LED off, high - LED on
#define DISP_PIN_E              0x04
#define DISP_PIN_RW             0x02 // low to write, high to read
#define DISP_PIN_RS             0x01 // low for cmd reg, high for data reg

#define SECOND_LINE_START_ADDR  0x40

//#define E_PULSE                 500ns = 0.5us

Display::Display() {
    // TODO Auto-generated constructor stub
    mFd          = -1;
    mByte        = 0x00;
    mCurrentAddr  = 0x00;
    mBckLight    = true;
    mDispOn      = false;
    mCursorOn    = false;
    mBlinkOn     = false;
    mInitialized = false;
    mDispShift   = false;
    mBitMode     = DISP_8_BIT_MODE;
    mLineMode    = DISP_1_LINE_MODE;
    mFont        = DISP_FONT_5x8;
    mAddrInc     = DISP_ADDR_INC;
}

Display::~Display() {
    // TODO Auto-generated destructor stub
}

void Display::turnBckLightOn() {
    uint8_t cmd = DISP_PIN_LED;
    wiringPiI2CWrite(mFd, cmd);
    mBckLight = true;
    DBG_INFO("Display backlight is on");
}

void Display::turnBckLightOff() {
    uint8_t cmd = 0x00;
    wiringPiI2CWrite(mFd, cmd);
    mBckLight = true;
    DBG_INFO("Display backlight is off");
}

void Display::toggleEnable() {
    mByte = mBckLight ? mByte | DISP_PIN_LED : mByte/* & ~DISP_PIN_LED*/; //TODO ovde ne mora mByte & ~DISP_PIN_LED, dovoljno je samo mByte
    wiringPiI2CWrite (mFd, mByte |  DISP_PIN_E); //TODO ovde ne treba ==
    wiringPiI2CWrite (mFd, mByte & ~DISP_PIN_E);

//    std::cout<< "mByte:" << std::bitset<8>(mByte) << std::endl;
}

void Display::sendCommand(uint8_t cmd) {
    switch(mBitMode) {
        case DISP_4_BIT_MODE:
            mByte = cmd & 0xf0;
            toggleEnable();
            mByte = (cmd << 4) & 0xf0;
            toggleEnable();
            break;
        case DISP_8_BIT_MODE:
            mByte = cmd;
            toggleEnable();
            break;
        default: break;
    }
}

DispErr Display::initialize(DispBitMode bitMode, DispLineMode lineMode, DispCharFont font) {

    uint8_t cmd;

    if(mInitialized) {
        return DISP_ERROR_DISP_ALREADY_INITIALIZED;
    }
    if(lineMode == DISP_2_LINE_MODE && font == DISP_FONT_5x10) {
        return DISP_ERROR_BAD_ARGUMENT;
    }

    mFd = wiringPiI2CSetup(I2C_ADDR);
    if(mFd == -1) {
        std::cout << "Display init failed!" << std::endl;
        return DISP_ERROR_INIT_FAILED;
    }

    wiringPiI2CWrite (mFd, 0x00);
    usleep(100);

    sendCommand(DISP_CMD_FUNC_SET | DISP_OPT_DATA_LENGTH_8);
    usleep(4100);   // more than 4.1ms
    sendCommand(DISP_CMD_FUNC_SET | DISP_OPT_DATA_LENGTH_8);
    usleep(100);    // more than 100us
    sendCommand(DISP_CMD_FUNC_SET | DISP_OPT_DATA_LENGTH_8);
    usleep(37);

    cmd = (bitMode == DISP_4_BIT_MODE) ? (DISP_CMD_FUNC_SET) : (DISP_CMD_FUNC_SET | DISP_OPT_DATA_LENGTH_8);
    sendCommand(cmd);
    usleep(37);
    mBitMode = bitMode;

    cmd = DISP_CMD_FUNC_SET;
    if(lineMode == DISP_2_LINE_MODE) {
        cmd |= DISP_OPT_LINES_NUM_2;
        mLineMode = DISP_2_LINE_MODE;
    }
    if(font == DISP_FONT_5x10) {
        cmd |= DISP_OPT_DOTS_5x10;
        mFont = DISP_FONT_5x10;
    }
    sendCommand(cmd);
    usleep(37);

    sendCommand(DISP_CMD_ON_OFF);
    usleep(37);

    sendCommand(DISP_CMD_CLEAR);
    usleep(37);

    sendCommand(DISP_CMD_ENTRY_MODE | DISP_OPT_DIR_INCREMENT);
    usleep(37);

    sendCommand(DISP_CMD_ENTRY_MODE | DISP_OPT_DIR_INCREMENT);
    usleep(37);

    mInitialized = true;

    return DISP_ERROR_OK;
}

DispErr Display::setBitMode(DispBitMode mode) {

    if(!mInitialized) {
        return DISP_ERROR_DISP_NOT_INITIALIZED;
    }

    uint8_t cmd;
    cmd = (mode == DISP_4_BIT_MODE) ? (DISP_CMD_FUNC_SET) : (DISP_CMD_FUNC_SET | DISP_OPT_DATA_LENGTH_8);
    sendCommand(cmd);
    usleep(37);
    mBitMode = mode;

    return DISP_ERROR_OK;
}

DispErr Display::setDispOn() {

    if(!mInitialized) {
        return DISP_ERROR_DISP_NOT_INITIALIZED;
    }
    uint8_t cmd;
    cmd = DISP_CMD_ON_OFF | DISP_OPT_DISP_ON;
    if(mCursorOn) {
        cmd |= DISP_OPT_CURSOR_ON;
    }
    if(mBlinkOn) {
        cmd |= DISP_OPT_BLINK_ON;
    }
    sendCommand(cmd);
    usleep(37);
    mDispOn = true;

    return DISP_ERROR_OK;
}

DispErr Display::setDispOff() {

    if(!mInitialized) {
        return DISP_ERROR_DISP_NOT_INITIALIZED;
    }
    uint8_t cmd;
    cmd = DISP_CMD_ON_OFF;
    if(mCursorOn) {
        cmd |= DISP_OPT_CURSOR_ON;
    }
    if(mBlinkOn) {
        cmd |= DISP_OPT_BLINK_ON;
    }
    sendCommand(cmd);
    usleep(37);
    mDispOn = false;

    return DISP_ERROR_OK;
}

DispErr Display::setCursorOn() {

    if(!mInitialized) {
        return DISP_ERROR_DISP_NOT_INITIALIZED;
    }
    uint8_t cmd;
    cmd = DISP_CMD_ON_OFF | DISP_OPT_CURSOR_ON;
    if(mDispOn) {
        cmd |= DISP_OPT_DISP_ON;
    }
    if(mBlinkOn) {
        cmd |= DISP_OPT_BLINK_ON;
    }
    sendCommand(cmd);
    usleep(37);
    mCursorOn = true;

    return DISP_ERROR_OK;
}

DispErr Display::setCursorOff() {

    if(!mInitialized) {
        return DISP_ERROR_DISP_NOT_INITIALIZED;
    }
    uint8_t cmd;
    cmd = DISP_CMD_ON_OFF;
    if(mDispOn) {
        cmd |= DISP_OPT_DISP_ON;
    }
    if(mBlinkOn) {
        cmd |= DISP_OPT_BLINK_ON;
    }
    sendCommand(cmd);
    usleep(37);
    mCursorOn = false;

    return DISP_ERROR_OK;
}

DispErr Display::setBlinkOn() {

    if(!mInitialized) {
        return DISP_ERROR_DISP_NOT_INITIALIZED;
    }

    uint8_t cmd;
    cmd = DISP_CMD_ON_OFF | DISP_OPT_BLINK_ON;
    if(mDispOn) {
        cmd |= DISP_OPT_DISP_ON;
    }
    if(mCursorOn) {
        cmd |= DISP_OPT_CURSOR_ON;
    }
    sendCommand(cmd);
    usleep(37);
    mBlinkOn = true;

    return DISP_ERROR_OK;
}

DispErr Display::setBlinkOff() {

    if(!mInitialized) {
        return DISP_ERROR_DISP_NOT_INITIALIZED;
    }

    uint8_t cmd;
    cmd = DISP_CMD_ON_OFF;
    if(mDispOn) {
        cmd |= DISP_OPT_DISP_ON;
    }
    if(mCursorOn) {
        cmd |= DISP_OPT_CURSOR_ON;
    }
    sendCommand(cmd);
    usleep(37);
    mBlinkOn = false;

    return DISP_ERROR_OK;
}

DispErr Display::setEntryModeDispShift() {
    if(!mInitialized) {
        return DISP_ERROR_DISP_NOT_INITIALIZED;
    }
    if(mDispShift) {
        return DISP_ERROR_OK;
    }

    uint8_t cmd;
    cmd = DISP_CMD_ENTRY_MODE | DISP_OPT_DISP_SHIFT_ON;
    if(mAddrInc == DISP_ADDR_INC) {
        cmd |= DISP_OPT_DIR_INCREMENT;
    }
    sendCommand(cmd);
    usleep(37);
    mDispShift = true;

    return DISP_ERROR_OK;
}

DispErr Display::writeChar(char c) {

    mCurrentAddr++;
//    if(mCurrentAddr > 0x0f) {
//        setEntryModeDispShift();
//    }

    switch(mBitMode) {
        case DISP_4_BIT_MODE:
            mByte = (c & 0xf0) | DISP_PIN_RS;
            toggleEnable();
            mByte = ((c << 4) & 0xf0) | DISP_PIN_RS;
            toggleEnable();
            break;
        case DISP_8_BIT_MODE:
//            mByte = cmd;
//            std::cout<< "mByte3:" << std::bitset<8>(mByte) << std::endl;
//            toggleEnable();
            break;
        default: break;
    }

    return DISP_ERROR_OK;
}

/*
 * @param line is line number, from 1 to 2
 * @param pos is position in the line, from 1 to 40
 *
 * */
DispErr Display::setCursorPosition(uint8_t line, uint8_t pos) {
    uint8_t cmd;
    uint8_t addr;

    if(line < 1 || line > 2 || pos < 1 || pos > 40) {
        return DISP_ERROR_BAD_ARGUMENT;
    }

    addr = (line - 1) * SECOND_LINE_START_ADDR + pos - 1;


    cmd = DISP_CMD_DDRAM_SET | addr;
    sendCommand(cmd);
    usleep(37);
    return DISP_ERROR_OK;
}

DispErr Display::writeText(const char* text, uint8_t size) {
    uint8_t i;

    for(i=0; i<size; i++) {
        writeChar(*(text + i));
    }

    return DISP_ERROR_OK;
}


