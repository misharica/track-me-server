/*
 * DirectionApiProxy.cpp
 *
 *  Created on: Nov 30, 2020
 *      Author: rtrk
 */

#include "DirectionApiProxy.h"
#include <UserLocation.h>
#include <curl/curl.h>
#include <logs.h>
#include <rapidjson/document.h>
#include <unistd.h>
#include <functional>


/**********************************************************************/
/* this is a mechanism that allows us to pass c++ member fnc to C API */
/**********************************************************************/
template <typename T>
struct Callback;

template <typename Ret, typename... Params>
struct Callback<Ret(Params...)> {
   template <typename... Args>
   static Ret callback(Args... args) {
      return func(args...);
   }
   static std::function<Ret(Params...)> func;
};

template <typename Ret, typename... Params>
std::function<Ret(Params...)> Callback<Ret(Params...)>::func;

typedef size_t (*callback_t)(void*, size_t, size_t, void*);

/**********************************************************************/


DirectionApiProxy::DirectionApiProxy() {
    // TODO Auto-generated constructor stub
    curl_global_init(CURL_GLOBAL_DEFAULT);

    mResponse.size = 0;
    mResponse.data = NULL;

}

DirectionApiProxy::~DirectionApiProxy() {
    // TODO Auto-generated destructor stub
    curl_global_cleanup();
}

void DirectionApiProxy::updateDirections(UserLocation *location) {

    CURL *curl;
    CURLcode res;

    DBG_INFO("curl_easy_init");
    curl = curl_easy_init();
    if(curl) {

        UrlBuilder urlBuilder = UrlBuilder(location);
        curl_easy_setopt(curl, CURLOPT_URL, urlBuilder.getUrl());

        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);

        /* send all data to this function  */
        Callback<size_t(void*, size_t, size_t, void*)>::func = std::bind(&DirectionApiProxy::writeDataCallback, this,
                std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4);
        callback_t func = static_cast<callback_t>(Callback<size_t(void*, size_t, size_t, void*)>::callback);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, func);

        /* we don't need CURLOPT_WRITEDATA since we are using member function */

        DBG_INFO("curl_easy_perform");
        res = curl_easy_perform(curl);

        if(res != CURLE_OK) {
            DBG_ERROR("curl_easy_perform() failed: %s", curl_easy_strerror(res));
        }
        else {
            /* data is valid here */
            DBG_VERBOSE("\n%s", mResponse.data);
            parseJson(mResponse.data);
        }

        curl_easy_cleanup(curl);

        free(mResponse.data);
        mResponse.data = NULL;
        mResponse.size = 0;
    }

}

size_t DirectionApiProxy::writeDataCallback(void *data, size_t size, size_t nmemb, void *userp) {

    size_t realsize = size * nmemb;
    char *ptr = (char*)realloc(mResponse.data, mResponse.size + realsize + 1);
    if(ptr == NULL) {
        DBG_ERROR("We are out of memory!!!");
        return 0;
    }

    mResponse.data = ptr;
    memcpy((mResponse.data + mResponse.size), data, realsize);
    mResponse.size += realsize;
    mResponse.data[mResponse.size] = 0;

    return realsize;

}

DirectionsApiError DirectionApiProxy::parseJson(const char* json) {

    // 1. Parse a JSON string into DOM.
    rapidjson::Document document;
    document.Parse(json);

    if(!document.IsObject()) {
        DBG_ERROR("The root is not an object");
        return DIR_API_ERR_JSON_PARSING_FAILED;
    }

    const rapidjson::Value& route    = document["routes"][0];
    const rapidjson::Value& leg      = route["legs"][0];
    const rapidjson::Value& distance = leg["distance"];
    const rapidjson::Value& duration = leg["duration"];
    const rapidjson::Value& endLoc   = leg["end_location"];
    const rapidjson::Value& startLoc = leg["start_location"];

    mDirections.distance.text  = distance["text"].GetString();
    mDirections.distance.value = distance["value"].GetInt();

    mDirections.duration.text  = duration["text"].GetString();
    mDirections.duration.value = duration["value"].GetInt();

    mDirections.endAddr        = leg["end_address"].GetString();
    mDirections.endLoc.lat     = endLoc["lat"].GetDouble();
    mDirections.endLoc.lng     = endLoc["lng"].GetDouble();

    mDirections.startAddr        = leg["start_address"].GetString();
    mDirections.startLoc.lat     = startLoc["lat"].GetDouble();
    mDirections.startLoc.lng     = startLoc["lng"].GetDouble();

    DBG_VERBOSE("Distance text[%s]", mDirections.distance.text.c_str());
    DBG_VERBOSE("Distance value[%d]", mDirections.distance.value);

    DBG_VERBOSE("Duration text[%s]", mDirections.duration.text.c_str());
    DBG_VERBOSE("Duration value[%d]", mDirections.duration.value);

    DBG_VERBOSE("End address [%s]", mDirections.endAddr.c_str());
    DBG_VERBOSE("End location latitude [%.7f]", mDirections.endLoc.lat);
    DBG_VERBOSE("End location longitude [%.7f]", mDirections.endLoc.lng);

    DBG_VERBOSE("Start address [%s]", mDirections.startAddr.c_str());
    DBG_VERBOSE("Start location latitude [%.7f]", mDirections.startLoc.lat);
    DBG_VERBOSE("Start location longitude [%.7f]", mDirections.startLoc.lng);

    return DIR_API_ERR_OK;
}

DirectionApiProxy::UrlBuilder::UrlBuilder(UserLocation *location) {

    size_t urlSize = strlen(API_URL_EXAMPLE) + 1; //mozda +1
    mUrl = (char*)calloc(urlSize, sizeof(char));
    if (mUrl == NULL) {
        return;
    }
    sprintf(mUrl, API_URL_TEMPLATE,
            location->getLatitude(), location->getLongitude(),
            48.8312381, 2.232717);

}

DirectionApiProxy::UrlBuilder::~UrlBuilder() {

    free(mUrl);
    mUrl = NULL;

}

