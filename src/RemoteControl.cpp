/*
 * RemoteControl.cpp
 *
 *  Created on: Oct 11, 2020
 *      Author: rtrk
 */

#include <RemoteControl.h>
#include <time.h>
#include <iostream>
#include <wiringPi.h>
#include <functional>

/**********************************************************************/
/* this is a mechanism that allows us to pass c++ member fnc to C API */
/**********************************************************************/
template <typename T>
struct Callback;

template <typename Ret, typename... Params>
struct Callback<Ret(Params...)> {
   template <typename... Args>
   static Ret callback(Args... args) {
      return func(args...);
   }
   static std::function<Ret(Params...)> func;
};

template <typename Ret, typename... Params>
std::function<Ret(Params...)> Callback<Ret(Params...)>::func;

typedef void (*callback_t)();

/**********************************************************************/

RemoteControl::RemoteControl() {
    // TODO Auto-generated constructor stub
    Callback<void(void)>::func = std::bind(&RemoteControl::printRc, this);
    callback_t func = static_cast<callback_t>(Callback<void(void)>::callback);
    wiringPiISR (HX1838_OUTPUT_PIN, INT_EDGE_FALLING, func);
    mState = NEC_Idle;
    mBitCnt = 0;
}

RemoteControl::~RemoteControl() {
    // TODO Auto-generated destructor stub
}

void RemoteControl::printRc() {

    long int        diff;
    struct timespec now;

    switch(mState) {
        case NEC_Idle:
            std::cout << "falling edge detected, possible start..." << std::endl;
            clock_gettime(CLOCK_REALTIME, &mLast);
            mState = NEC_DetectingStart;
            break;
        case NEC_DetectingStart:
            clock_gettime(CLOCK_REALTIME, &now);
            diff = now.tv_nsec - mLast.tv_nsec;
            if(diff < 0) {
                diff += 1000000000;
            }
            if(diff > 12000000) { //start
                mState = NEC_Reading;
                std::cout << "start detected..." << std::endl;
            }
            else { //repeat
                mState = NEC_Idle;
                std::cout << "repeat detected..." << std::endl;
            }
            mLast = now;
            break;
        case NEC_Reading:
            clock_gettime(CLOCK_REALTIME, &now);
            diff = now.tv_nsec - mLast.tv_nsec;
            if(diff < 0) {
                diff += 1000000000;
            }
            if(diff < 1687500) {
                std::cout << "0" << " ";
            }
            else if (diff > 1687500) {
                std::cout << "1 ";
            }
            mBitCnt++;
            if(mBitCnt == 32 ) {
                mBitCnt = 0;
                std::cout << std::endl;
                mState = NEC_Idle;
            }
            mLast = now;
            break;
        default: break;
    }
}

