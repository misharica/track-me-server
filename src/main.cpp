//============================================================================
// Name        : CrossCompHelloWorld.cpp
// Author      : Milos
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <wiringPi.h>
#include <Display.h>
#include <RemoteControl.h>
#include <Server.h>
#include <ControlModule.h>
#include <DisplayControl.h>

#include <cstdlib>
// getchar
#include <stdio.h>
#include <logs.h>

#include <unistd.h> // for usleep

using namespace std;

#define BACKLOG     10
#define SRV_PORT    "17001"

int main() {

    wiringPiSetup();

    RemoteControl *rc = new RemoteControl();

    ControlModule *controlModule  = new ControlModule();

	Server *srv = new Server();
	controlModule->attachServer(srv);

    DisplayControl *dispCtrl  = new DisplayControl();
    controlModule->attachDisplayControl(dispCtrl);

    Display *disp = new Display();
    dispCtrl->attachDisplay(disp);

    controlModule->start();

    if(srv->start(SRV_PORT) != SRV_ERR_OK) {
        exit(1);
    }
    if(dispCtrl->start() != DISP_CTRL_ERR_OK) {
        exit(1);
    }

      char c;
      while((c=getchar())!= '.') {
          if(c == '\n') {
              continue;
          }
    //        cout << c << endl;
          if(c == 'q') {
              break;
          }
            disp->writeChar(c);
      }

      disp->turnBckLightOff();
      return -1;


	while(1) {
	    usleep(1000000);
	}

///*************************************************************/
//	wiringPiSetup();
//
//
//
//	RemoteControl *rc = new RemoteControl();
//
//	Display *disp = new Display();
//	disp->initialize(DISP_4_BIT_MODE, DISP_2_LINE_MODE, DISP_FONT_5x8);
//	disp->setDispOn();
//	disp->setBlinkOn();
//
//	char c;
//	while((c=getchar())!= '.') {
//	    if(c == '\n') {
//	        continue;
//	    }
////	    cout << c << endl;
//	    if(c == 'q') {
//	        break;
//	    }
//        disp->writeChar(c);
//	}
//
//	disp->turnBckLightOff();
//	return -1;
//
////	pazi, ovaj pin se koristi za senzor!!!
////    pinMode (25, OUTPUT) ;
////    for (;;)
////    {
////        digitalWrite (25, HIGH) ; delay (500) ;
////        digitalWrite (25,  LOW) ; delay (500) ;
////    }
///*************************************************************/


	return 0;
}
